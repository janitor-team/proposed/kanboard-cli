Source: kanboard-cli
Section: devel
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: ChangZhuo Chen (陳昌倬) <czchen@debian.org>
Build-Depends: debhelper-compat (= 13),
               asciidoc-base,
               dh-python,
               python3-all (>= 3.4),
               python3-cliff,
               python3-kanboard,
               python3-mock,
               python3-pbr,
               python3-setuptools,
               xmlto,
Standards-Version: 3.9.8
Homepage: https://github.com/kanboard/kanboard-cli
Vcs-Git: https://salsa.debian.org/python-team/packages/kanboard-cli.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/kanboard-cli

Package: kanboard-cli
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
Description: kanboard command line client
 Kanboard (https://kanboard.net/) is project management software that
 focuses on the Kanban methodology. It has the following features:
 .
  * Visualize your work
  * Limit your work in progress to be more efficient
  * Customize your boards according to your business activities
  * Multiple projects with the ability to drag and drop tasks
  * Reports and analytics
  * Fast and simple to use
  * Access from anywhere with a modern browser
  * Plugins and integrations with external services
  * Free, open source and self-hosted
  * Super simple installation
 .
 kanboard-cli is a command line client for kanboard application.
